---
focus: src/Bicycle.ts
---
### Summary
* Inheritance solves the problem of related types that share a great deal of common behavior but differ across some dimension;
* The best way to create an abstract superclass is by pushing code up from concrete subclasses;
* Abstract superclasses use the template method pattern to invite inheritors to supply specializations, and they use hook methods to allow these inheritors to contribute these specializations without being forced to send super;
* Well-designed inheritance hierarchies are easy to extend with new subclasses, even for programmers who know very little about the application;
