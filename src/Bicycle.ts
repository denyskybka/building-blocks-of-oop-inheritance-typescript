abstract class Bicycle {
  constructor(opts) {
    this.size = opts.size;
    this.chain = opts.chain;
    this.tireSize = opts.tireSize;
    this.postInitialize(opts);
  }

  protected postInitialize() {};

  spares() {
    return {
      tireSize: this.tireSize,
      chain: this.chain,
      ...this.localSpares()}
  }
}

class RoadBike extends Bicycle {
  protected postInitialize(opts) {
    this.tapeColor = opts.tapeColor;
  };

  protected localSpares() {
    return { tapeColor: this.tapeColor };
  }
}

class MountainBike extends Bicycle {
  protected postInitialize(opts) {
    this.frontShock = opts.frontShock;
  };

  protected localSpares() {
    return { frontShock: this.frontShock };
  }
}
